(function() {
    'use strict';

    /**
     * @ngdoc controller
     * @name app.limalinks.generic-ui.controller:ChangeParamsController
     *
     * @description
     * This is a controller for controlling Parameter and Chart Data Generation
     */

    angular
        .module('app.limalinks.analytics-ui')
        .controller('ChangeParamsController', ChangeParamsController);

    /* @ngInject */
    function ChangeParamsController($q, $state, $mdDialog, $scope, data, UIService, AlertService, ChipService) {
        var vm = this;
        vm.editMode = false;
        vm.references = []; //Used for looking up the names of other types of objects like provinces
        vm.childSets = []; //Used for displaying childsets to user
        vm.childSetHeadings = []; //Used for displaying childsets to user
        vm.inputs = [];
        vm.selects = [];
        vm.dateControls = [];
        vm.predefinedSelects = [];
        vm.fields = [];
        vm.model = data.params;
        vm.selected = data.selected;
        vm.mode = data.mode;
        vm.object = {}; //View's scope object
        vm.object_refs = [];
        vm.ids = [];
        vm.params = [];

        //Controller's API
        vm.activate = activate;
        vm.cancel = cancel;
        vm.createDetailFields = createDetailFields;
        vm.createFields = createFields;
        vm.generateChildrenSets = generateChildrenSets;
        vm.hide = hide;
        vm.setReference = setReference;
        vm.initAddOrEditMode = initAddOrEditMode;



        vm.activate();

        /////////////////////////

        /**
         * @ngdoc method
         * @name createFields
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method traverses the model information received from the API and generates the UI controls for the dialog in add or edit mode
         *
         *
         *

        */

        function createFields() {

          return $q(function(resolve, reject) {

                angular.forEach(vm.model, function(param){


                  //Debug



                    if(param.hasOwnProperty("data_source")){
                      var refs = [];

                      var ref_model = param.data_source.substring(4, param.data_source.length-1);
                      vm.params.push({name:param.name, type:"multiple"});
                      vm.selects.push({name: param.name, label: param.label, ref_model: param.data_source});
                      console.info(param.data_source);
                      console.info(vm.model);



                    }

                    else if (param.type == "isotimestamp") {


                      vm.params.push({name:param.name, type:"date"});
                      vm.dateControls.push({name: param.name, label: param.label, refs: refs});
                    }
                    else if (param.hasOwnProperty("choices")) {


                      vm.params.push({name:param.name, type:"multiple"});

                      var refs = param.choices;
                      vm.predefinedSelects.push({name: param.name, label: param.label, refs: refs });
                    }
                    else {
                      //vm.inputs.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length});
                    }


                });

                resolve (vm);
            });

        }


        /**
         * @ngdoc method
         * @name createDetailFields
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method traverses the model information received from the API and generates the information to display on the dialog in view mode
         *
         *
         *

        */

        function createDetailFields() {

          return $q(function(resolve, reject) {

                angular.forEach(vm.model, function(config, column){

                  if(config.hasOwnProperty("detail_index")){

                    if(config.type == "LLForeignKey"){
                      var refs = [];
                      var ref_model = "";
                      var objectType = config.data_source.substring(4, config.data_source.length-2);

                      switch(objectType) {
                          case "province":
                              refs = vm.provinces;
                              break;
                          case "district":
                              refs = vm.districts;
                              break;
                          case "town":
                              refs = vm.towns;
                              break;
                          case "ward":
                              refs = vm.wards;
                              break;
                          case "market":
                              refs = vm.markets;
                              break;
                          case "packaging":
                              refs = vm.packagings;
                              break;
                          case "crop":
                              refs = vm.crops;
                              break;
                          default:
                              console.log("error");
                      }
                      vm.fields.push({type: config.type, name: column, detail_index:config.detail_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: refs});
                    }
                    else {
                      vm.fields.push({type: config.type,name: column, detail_index:config.detail_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length});
                    }

                  }
                });

                resolve (vm);
            });

        }


        /**
         * @ngdoc method
         * @name setReference
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method sets the reference on the model because ng-model does not provide the ability to be dynamically updated
         *
         *
         *

        */

        function setReference (select){
          vm.object[select.name] = vm.object.ref;
          console.log(select);

        }


        /**
         * @ngdoc method
         * @name generateChildrenSets
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method goes through the set fields and matches them with the references arrays to display user friendly lists to the user
         *
         *
         *

        */

        function generateChildrenSets (){
          for(var field in vm.object){
            if(field.substring(field.indexOf("_")+1, field.length) == "set"){
              var index = field.substring(0, field.indexOf("_"))+"s";
              vm.childSetHeadings.push(index);
              var childrenArray = vm[index];
              var children = []; // storing this object's actual children
              angular.forEach(childrenArray, function(child){
                if(child[vm.model.objectType.toLowerCase()+"_ref"]==vm.object.id){
                  children.push(child);


                }
              });

              vm.childSets.push(children);
              vm.object[field] = children;

            }

          }
          console.log(vm.childSets);

        }


        /**
         * @ngdoc method
         * @name activate
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This is the initialisation method for the controller and it executes all functions that are needed when bootstrapping the view
         *
         *
         *

        */

        function activate() {
            console.log(data);
            vm.initAddOrEditMode();
            vm.createDetailFields();
            vm.createFields();







        }



        /**
         * @ngdoc method
         * @name initAddOrEditMode
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method sets the mode to add or edit when the view initialises. It also sets the scope object
         *
         *
         *

        */

        function initAddOrEditMode(){

          //If an item has been selected, then we are in edit mode and we must bind the fields of the view the selected object
          console.log(vm.mode);
          if(vm.mode == "edit"){
            vm.object = vm.selected[0];

          }
          else {
            vm.object = {};
            vm.selected = [];
            vm.editMode = true;
          }
        }

        /**
         * @ngdoc method
         * @name toggleMode
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method switches the views mode between add and edit mode
         *
         *
         *

        */

        function toggleMode(){
          if(vm.editMode){
            vm.editMode = false;
          }
          else {
            vm.editMode = true;
          }
        }

        /**
         * @ngdoc method
         * @name hide
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method dismisses the modal and returns the scope object to the calling controller
         *
         *
         *

        */

        function hide() {
          var chartParams = {}; //will be used to query API
          var chartData = [];

          var paramInstance = [];
          angular.forEach(vm.params, function(param){
            var index = param.name;
            paramInstance = vm[index];
            //if type is multiple, flatten array
            if (param.type =="multiple"){
              if(typeof paramInstance[0] == "object"){ //checking if first param value is object
                paramInstance = paramInstance.map(function(param){
                  param = param.id;
                  return param;
                }).join(); //join flattens array into comma separated values
              }
              else {
                paramInstance=paramInstance.join();
              }


            }

            // if type is date, change to unix timestamp
            else if(param.type == "date"){
              paramInstance = paramInstance.toISOString();
              //paramInstance = moment(paramInstance).unix()*1000;
            }
            chartParams[param.name] = paramInstance


          });

          vm.resource = UIService.query(chartParams, data.url);
          vm.resource.then(
            function(res){


              var chart = res.data.data;
              chart.params = chartParams;
              console.info(chart);
              $mdDialog.hide(chart);
            },
            function(err){

            }
          );



        }

        /**
         * @ngdoc method
         * @name cancel
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method dismisses the modal and does not return the scope object to the calling controller
         *
         *
         *

        */

        function cancel() {
            $mdDialog.cancel();
        }

    }
})();
