(function() {
    'use strict';

    angular
        .module('app.limalinks.authentication')
        .service('AuthService', AuthService);

    /* @ngInject */
    function AuthService($q, $http, SessionService, limalinksSettings, RoleStore, $cookies, $rootScope, $state, triMenu, $window){
        //get base URL from settings
        var baseURL = limalinksSettings.API;
        var currentUser = SessionService.getUser();
        /**
        * Check whether the user is logged in
        * @returns boolean
        */
        this.isLoggedIn = function isLoggedIn(){
          return SessionService.getUser() !== null;
        };

        /**
        * Log in
        *
        * @param credentials
        * @returns {*|Promise}
        */
        this.logIn = function(credentials, callback){
          $http
            .get(baseURL + '/v3/auth/login?username='+credentials.username+'&password='+credentials.password)
            .then(
              function(response){
                  var headers = response.headers;
                  var user = response.data.data;
                  $rootScope.views = response.data.views;
                  $rootScope.user = user;
                  bindStates($rootScope.views);
                  user.displayName = user.first_name + " " + user.last_name;
                  user.avatar = 'assets/images/avatars/avatar-5.png',
                  SessionService.setUser(user);
                  SessionService.setAccessToken("Basic " + btoa(credentials.username + ":" + credentials.password));
                  SessionService.getAccessToken();
                  callback(true);
              },
              function(error){
                  callback(false);
              }
          );
        };

        /**
        * Log out
        *
        * @returns {*|Promise}
        */
        this.logOut = function(){
          return $http
            .get(baseURL + '/v3/auth/logout')
            .then(function(response){

              // Destroy SessionService in the browser
              SessionService.destroy();
              $state.go("authentication.login")
              $window.location.reload();
            });

        };

        this.hasPermission = function(permission) {
            $rootScope.user = SessionService.getUser();
            console.info($rootScope.user);
            if($rootScope.user !== null){
                bindStates($rootScope.user.views);
            }

            /*var deferred = $q.defer();
            var hasPermission = false;

            // check if user has permission via its roles
            angular.forEach(currentUser.roles, function(role) {
                // check role exists
                if(RoleStore.hasRoleDefinition(role)) {
                    // get the role
                    var roles = RoleStore.getStore();

                    if(angular.isDefined(roles[role])) {
                        // check if the permission we are validating is in this role's permissions
                        if(-1 !== roles[role].validationFunction.indexOf(permission)) {
                            hasPermission = true;
                        }
                    }
                }
            });

            // if we have permission resolve otherwise reject the promise
            if(hasPermission) {
                deferred.resolve();
            }
            else {
                deferred.reject();
            }

            // return promise
            return deferred.promise;
            */
        }

        this.getUsers = function() {
            return $http.get('app/permission/data/users.json');
        }

        function bindStates (views) {
            var index = 0;
            views.forEach(function bind(stateData) {


                // add new states
                var parent = stateData.section.toLowerCase();
                var endpoint = stateData.label.toLowerCase();
                views[index].state = 'triangular.'+parent+"-"+endpoint;
                angular.module('app').$stateProvider.state({

                    name: 'triangular.'+parent+"-"+endpoint,
                    url: '/'+parent+'/'+endpoint,
                    templateUrl: 'app/limalinks/generic-ui/ui.tmpl.html',
                    controller: 'UIController',
                    controllerAs: 'vm',
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });
                index++;
            });

            // createMenu(views);
            $rootScope.views = views;
            $rootScope.user.views = views;
            SessionService.setUser($rootScope.user);
            createMenu($rootScope.views);
            // go to default state
            $state.go($rootScope.views[0].state);


        }



        function createMenu (views) {
            //First create containers
            // console.info(views);
            var containers = [];
            var menus = [];
            angular.forEach(views, function(view){
              var menu = {};
              if(menus.indexOf(view.section)==-1){
                menu = {
                  name:view.section,
                  icon: view.icon || 'zmdi zmdi-folder',
                  type: 'dropdown',
                  //permission: 'viewAdminUI',
                  priority: 1.1,
                  children:[]

                };
                //containters.push(view.section);
                angular.forEach(views, function(menuItem){
                  var child = {};
                  if (menuItem.section == view.section){
                    child = {
                      name: menuItem.label,
                      state: menuItem.state,
                      type: 'link'
                    }
                    menu.children.push(child);

                  }
                });
                menus.push(menu.name);
                triMenu.addMenu(menu);
              }

            });



        }



  }

})();
