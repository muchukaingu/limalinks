(function() {
    'use strict';

    /**
     * @ngdoc controller
     * @name app.limalinks.generic-ui.controller:AddObjectDialogController
     *
     * @description
     * This is a controller for the dialog for adding Objects
     */

    angular
        .module('app.limalinks.generic-ui')
        .controller('AddDialogController', AddDialogController);

    /* @ngInject */
    function AddDialogController($q, $state, $mdDialog, $scope, data, UIService, AlertService) {
        var vm = this;
        vm.editMode = false;
        vm.references = []; //Used for looking up the names of other types of objects like provinces
        vm.childSets = []; //Used for displaying childsets to user
        vm.childSetHeadings = []; //Used for displaying childsets to user
        vm.inputs = [];
        vm.selects = [];
        vm.dateControls = [];
        vm.predefinedSelects = [];
        vm.booleanSelects = [];
        vm.fields = [];
        vm.chips = [];
        vm.arrayFields = [];
        vm.model = data.model;
        vm.tabs = data.tabs;
        // console.info(vm.tabs);
        vm.selected = data.selected;
        vm.mode = data.mode;
        vm.object = {}; //View's scope object
        vm.object_refs = [];


        //Controller's API
        vm.activate = activate;
        vm.cancel = cancel;
        vm.createDetailFields = createDetailFields;
        vm.createFields = createFields;
        vm.getReferences = getReferences;
        vm.generateChildrenSets = generateChildrenSets;
        vm.hide = hide;
        vm.setReference = setReference;
        vm.initAddOrEditMode = initAddOrEditMode;



        vm.activate();

        /////////////////////////

        /**
         * @ngdoc method
         * @name createFields
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method traverses the model information received from the API and generates the UI controls for the dialog in add or edit mode
         *
         *
         *

        */

        function createFields() {

          return $q(function(resolve, reject) {

                angular.forEach(vm.model, function(config, column){


                  //Debug
                  if(column == "type"){
                    // console.log(config);
                  }
                  if(config !==null && config.hasOwnProperty("editor_index")){

                    if(config.type == "LLForeignKey"){

                      var refs = [];
                      var ref_model = "";
                      var objectType = config.data_source.substring(4, config.data_source.length-2);
                      vm.selects.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: objectType+'s', tab:config.tab_label});
                      console.info(vm.selects);

                    }
                    else if (config.hasOwnProperty("choices")){
                      var refs = config.choices;

                      angular.forEach(refs, function(ref){
                        ref = ref.reduce(function(acc, cur, i) {

                          acc[i] = cur;
                          return acc;
                        }, {});
                        // console.log(ref);
                      });
                      // console.log(config.choices);
                      vm.predefinedSelects.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: refs, tab:config.tab_label});

                    }
                    else if (config.type == "BooleanField"){
                      var refs = [
                        {value:"true",label:"True"},
                        {value:"false", label: "False"}
                      ];
                      vm.booleanSelects.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: refs, tab:config.tab_label});

                    }
                    else if (config.type == "DateTimeField") {
                      vm.object[column] = (vm.object[column]==undefined)?new Date(Date.now()):new Date(vm.object[column]);
                      vm.dateControls.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, tab:config.tab_label});
                    }
                    else if (config.type == "LLManyToManyField" || config.type == "Set") {
                      if(vm.mode == "add"){
                          vm.object[column]=[];
                      }

                      vm.chips.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, data_source: config.data_source, tab:config.tab_label});
                    }
                    else {
                      vm.inputs.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, tab:config.tab_label});
                    }

                  }
                });

                resolve (vm);
            });

        }


        /**
         * @ngdoc method
         * @name createDetailFields
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method traverses the model information received from the API and generates the information to display on the dialog in view mode
         *
         *
         *

        */

        function createDetailFields() {

          return $q(function(resolve, reject) {

                angular.forEach(vm.model, function(config, column){

                  if(config !==null && config.hasOwnProperty("detail_index")){

                    if(config.type == "LLForeignKey"){
                      var refs = [];
                      var ref_model = "";

                      vm.fields.push({type: config.type, name: column, detail_index:config.detail_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: refs, tab:config.tab_label});
                    }
                    else if (config.type == "Set" || config.type == "LLManyToManyField" || config.hasOwnProperty("data_source")) {
                      vm.arrayFields.push({type: config.type,name: column, detail_index:config.detail_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, tab:config.tab_label});
                    }
                    else {
                      vm.fields.push({type: config.type,name: column, detail_index:config.detail_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, tab:config.tab_label});
                    }

                  }
                });

                resolve (vm);
            });

        }


        /**
         * @ngdoc method
         * @name setReference
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method sets the reference on the model because ng-model does not provide the ability to be dynamically updated
         *
         *
         *

        */

        function setReference (select){
          vm.object[select.name] = vm.object.ref;
          // console.log(select);

        }


        /**
         * @ngdoc method
         * @name generateChildrenSets
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method goes through the set fields and matches them with the references arrays to display user friendly lists to the user
         *
         *
         *

        */

        function generateChildrenSets (){
          for(var field in vm.object){
            if(field.substring(field.indexOf("_")+1, field.length) == "set"){
              var index = field.substring(0, field.indexOf("_"))+"s";
              vm.childSetHeadings.push(index);
              var childrenArray = vm[index];
              var children = []; // storing this object's actual children
              angular.forEach(childrenArray, function(child){
                if(child[vm.model.objectType.toLowerCase()+"_ref"]==vm.object.id){
                  children.push(child);


                }
              });

              vm.childSets.push(children);
              vm.object[field] = children;

            }

          }
          // console.log(vm.childSets);

        }


        /**
         * @ngdoc method
         * @name activate
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This is the initialisation method for the controller and it executes all functions that are needed when bootstrapping the view
         *
         *
         *

        */

        function activate() {

            vm.initAddOrEditMode();
            vm.createDetailFields();
            //vm.getReferences();
            vm.controlsResource = vm.createFields();
            vm.controlsResource.then(
              function(){
                // console.log("showing modal");
              },
              function(){}
            );







        }

        /**
         * @ngdoc method
         * @name getReferences
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method initialises the references for the view from other entities e.g. provinces, districts etc
         *
         *
         *

        */

        function getReferences(referenceField) {
            /*vm.resource = ReferenceService.getReferences();
            return vm.resource.then(
              function(references){
                vm.provinces = references["provinces"];
                vm.districts = references["districts"];
                vm.towns = references["towns"];
                vm.wards = references["wards"];
                vm.markets = references["markets"];
                vm.packagings = references["packagings"];
                vm.crops = references["crops"];
                vm.advertisers = references["advertisers"];
                generateChildrenSets();





              },
              function(error){

              }
            )*/

            vm[referenceField+'s'] = [];



              // console.info("model", vm.model);
              vm.resource = UIService.query({},vm.model[referenceField].data_source);
              return vm.resource.then(
                  function(res){

                    vm[referenceField+'s'] = res.data.data;

                  },
                  function(err){

                  }
              )





        }

        /**
         * @ngdoc method
         * @name initAddOrEditMode
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method sets the mode to add or edit when the view initialises. It also sets the scope object
         *
         *
         *

        */

        function initAddOrEditMode(){

          //If an item has been selected, then we are in edit mode and we must bind the fields of the view the selected object
          // console.log(vm.mode);
          if(vm.mode == "edit"){
            vm.object = vm.selected[0];
            vm.editMode = true;


          }
          else if (vm.mode == "view"){
            vm.object = vm.selected;
            // console.log(vm.object);
          }
          else {
            vm.object = {};
            vm.selected = [];
            vm.editMode = true;
          }
        }

        /**
         * @ngdoc method
         * @name toggleMode
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method switches the views mode between add and edit mode
         *
         *
         *

        */

        function toggleMode(){
          if(vm.editMode){
            vm.editMode = false;
          }
          else {
            vm.editMode = true;
          }
        }

        /**
         * @ngdoc method
         * @name hide
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method dismisses the modal and returns the scope object to the calling controller
         *
         *
         *

        */

        function hide() {
          // console.log(vm.object);
          angular.forEach(vm.chips, function(chip){
            vm.object[chip.name] = vm.object[chip.name].map(function(item){
              item = item.id;
              return item;
            });
          });
          if(vm.mode=="edit"){
            console.info("selects", vm.selects);
            console.info("object", vm.object);
            angular.forEach(vm.selects, function(select){
              vm.object[select.name] = JSON.parse(vm.object[select.name]);
              console.info("edit mode", typeof vm.object[select.name]);
              console.info("edit mode", vm.object[select.name]);

              if(typeof vm.object[select.name]=="object"){
                console.info(vm.object[select.name].name);
                console.info(vm.object[select.name].id);
              }
              vm.object[select.name] = vm.object[select.name].id;

            });
          }
          $mdDialog.hide(vm.object);
        }

        /**
         * @ngdoc method
         * @name cancel
         * @methodOf app.limalinks.generic-ui.controller:AddObjectDialogController
         * @description
         * This method dismisses the modal and does not return the scope object to the calling controller
         *
         *
         *

        */

        function cancel() {
            $mdDialog.cancel();
        }
    }
})();
