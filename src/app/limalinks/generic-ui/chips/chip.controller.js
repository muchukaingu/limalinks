(function () {
  'use strict';

  /**
   * @ngdoc controller
   * @name app.limalinks.generic-ui.controller:ChipController
   *
   * @description
   * This is a generic controller for chips controls
   */

  // If we do not have CryptoJS defined; import it
  if (typeof CryptoJS == 'undefined') {
    var cryptoSrc = '//cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/md5.js';
    var scriptTag = document.createElement('script');
    scriptTag.setAttribute('src', cryptoSrc);
    document.body.appendChild(scriptTag);
  }

  angular
      .module('app.limalinks.generic-ui')
      .controller('ChipController', ChipController);

  function ChipController ($q, $timeout, $scope, UIService) {
    var self = this;
    var vm = $scope.vm;
    var pendingSearch, cancelSearch = angular.noop;
    var lastSearch;

    //self.allObjects = loadObjects();
    //self.objects = [self.allObjects[0]];
    self.asyncObjects = [];
    self.filterSelected = true;

    self.querySearch = querySearch;
    self.delayedQuerySearch = delayedQuerySearch;

    /**
     * @ngdoc method
     * @name querySearch
     * @methodOf app.limalinks.generic-ui.controller:ChipController
     * @description
     * This method searchs returned results from async search in delayedQuerySearch()
     *
     *
     *

    */

    function querySearch (criteria) {
      console.info(self.allObjects.filter(createFilterFor(criteria)));
      return criteria ? self.allObjects.filter(createFilterFor(criteria)) : [];
    }


   /**
    * @ngdoc method
    * @name delayedQuerySearch
    * @methodOf app.limalinks.generic-ui.controller:ChipController
    * @description
    * Async search for objects. Also debounce the queries; since the md-object-chips does not support this
    *
    *
    *

   */

    function delayedQuerySearch(criteria, data_source) {
      if ( !pendingSearch || !debounceSearch() )  {
        cancelSearch();

        return pendingSearch = $q(function(resolve, reject) {
          // Simulate async search... (after debouncing)
          cancelSearch = reject;


          var resource = UIService.query({search:criteria}, data_source);
          resource.then(
              function(res){
                self.allObjects = res.data.data;
                self.allObjects = self.allObjects.map(function (object) {
                  object.hasOwnProperty("name")?object._lowername = object.name.toLowerCase():{};
                  if(object.hasOwnProperty("first_name")){
                    object._lowername = (object.first_name+" "+object.last_name).toLowerCase();
                    object.name = object.first_name+" "+object.last_name;
                  }

                  return object;
                });
                console.info(self.allObjects);
                resolve( self.querySearch(criteria) );
              },
              function(err){

              }
          )








        });


      }

      return pendingSearch;
    }

    function refreshDebounce() {
      lastSearch = 0;
      pendingSearch = null;
      cancelSearch = angular.noop;
    }



   /**
    * @ngdoc method
    * @name debounceSearch
    * @methodOf app.limalinks.generic-ui.controller:ChipController
    * @description
    * Debounce if querying faster than 300ms
    *
    *
    *

   */

    function debounceSearch() {
      var now = new Date().getMilliseconds();
      lastSearch = lastSearch || now;

      return ((now - lastSearch) < 300);
    }


     /**
      * @ngdoc method
      * @name debounceSearch
      * @methodOf app.limalinks.generic-ui.controller:ChipController
      * @description
      * Create filter function for a query string
      *
      *
      *

     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(object) {
        console.info(object._lowername.indexOf(lowercaseQuery) != -1);
        return (object._lowername.indexOf(lowercaseQuery) != -1);
      };

    }

    function loadObjects() {

    }
  }

})();
