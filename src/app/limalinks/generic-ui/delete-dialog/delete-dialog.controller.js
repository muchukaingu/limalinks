(function() {
    'use strict';

    /**
     * @ngdoc controller
     * @name app.limalinks.generic-ui.controller:DeleteDialogController
     *
     * @description
     * This is a controller for the the deletion confirmation dialog
     */

    angular
        .module('app.limalinks.generic-ui')
        .controller('DeleteDialogController', DeleteDialogController);

    /* @ngInject */
    function DeleteDialogController($state, $mdDialog, data) {
        var vm = this;
        vm.selected = data.selected;
        vm.model = data.model;
        console.log(vm.selected);
        vm.cancel = cancel;
        vm.hide = hide;
        vm.object = {
            name: ''
        };

        /////////////////////////

        /**
         * @ngdoc method
         * @name hide
         * @methodOf app.limalinks.generic-ui.controller:DeleteDialogController
         * @description
         * This method confirms deletion of the selected items and hides the dialog
         *
         *
         *

        */

        function hide() {
            $mdDialog.hide(true);
        }

        /**
         * @ngdoc method
         * @name hide
         * @methodOf app.limalinks.generic-ui.controller:DeleteDialogController
         * @description
         * This method cancels deletion
         *
         *
         *

        */
        function cancel() {
            $mdDialog.cancel();
        }
    }
})();
