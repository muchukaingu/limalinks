(function() {
    'use strict';

    /**
     * @ngdoc controller
     * @name app.limalinks.generic-ui.controller:FilterParamsController
     *
     * @description
     * This is a controller for the dialog for filtering Objects
     */

    angular
        .module('app.limalinks.generic-ui')
        .controller('FilterParamsController', FilterParamsController);

    /* @ngInject */
    function FilterParamsController($q, $state, $mdDialog, $scope, data, UIService, AlertService) {
        var vm = this;
        vm.editMode = false;
        vm.references = []; //Used for looking up the names of other types of objects like provinces
        vm.childSets = []; //Used for displaying childsets to user
        vm.childSetHeadings = []; //Used for displaying childsets to user
        vm.inputs = [];
        vm.selects = [];
        vm.dateControls = [];
        vm.predefinedSelects = [];
        vm.fields = [];
        vm.chips = [];
        vm.arrayFields = [];
        vm.model = data.model;
        vm.selected = data.selected;
        vm.mode = data.mode;
        vm.object = data.filter; //View's scope object
        vm.object_refs = [];


        //Controller's API
        vm.activate = activate;
        vm.cancel = cancel;
        vm.createDetailFields = createDetailFields;
        vm.createFields = createFields;
        vm.getReferences = getReferences;
        vm.generateChildrenSets = generateChildrenSets;
        vm.hide = hide;
        vm.setReference = setReference;
        vm.initAddOrEditMode = initAddOrEditMode;



        vm.activate();

        /////////////////////////

        /**
         * @ngdoc method
         * @name createFields
         * @methodOf app.limalinks.generic-ui.controller:FilterParamsController
         * @description
         * This method traverses the model information received from the API and generates the UI controls for the filters
         *
         *
         *

        */

        function createFields() {

          return $q(function(resolve, reject) {

                angular.forEach(vm.model, function(config, column){


                  if (config !==null){
                    //console.info("config", config);

                    if(config.filter == true || config.filter_field == true){

                      if(config.type == "LLForeignKey"){

                        var refs = [];
                        var ref_model = "";
                        var objectType = config.data_source.substring(4, config.data_source.length-2);

                        vm.selects.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: objectType+'s'});

                        //If we are in edit mode set the value of the selected reference, so that the name is visible to the user
                        if(vm.selected && vm.selected.length == 1){
                            angular.forEach(vm.selects, function(select){
                              vm.object.ref=vm.object[select.name]; //temporal fix. It will be a problem when there are multiple reference types. Find a fix to allow dynamic ng-model
                            })
                        }


                      }
                      else if (config.hasOwnProperty("choices")){
                        var refs = config.choices;

                        angular.forEach(refs, function(ref){
                          ref = ref.reduce(function(acc, cur, i) {

                            acc[i] = cur;
                            return acc;
                          }, {});
                          console.log(ref);
                        });
                        console.log(config.choices);
                        vm.predefinedSelects.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: refs});

                      }
                      else if (config.type == "DateTimeField") {
                        if (vm.object[column] !==undefined){
                          var dates = vm.object[column].split(",");
                          vm.object[column+"_start"] = new Date(dates[0]);
                          vm.object[column+"_end"] = new Date(dates[1]);

                        }


                        vm.dateControls.push({name: column+"_start", editor_index:config.editor_index, label: "Start Date", required: config.required?"true":"false", maxlength:config.max_length});
                        vm.dateControls.push({name: column+"_end", editor_index:config.editor_index, label: "End Date", required: config.required?"true":"false", maxlength:config.max_length});
                      }
                      else if (config.type == "LLManyToManyField" || config.type == "Set") {
                        if(vm.mode == "add"){
                            vm.object[column]=[];
                        }

                        vm.chips.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, data_source: config.data_source});
                      }
                      else {
                        vm.inputs.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length});
                      }

                    }
                  }

                });

                resolve (vm);
            });

        }


        /**
         * @ngdoc method
         * @name createDetailFields
         * @methodOf app.limalinks.generic-ui.controller:FilterParamsController
         * @description
         * This method traverses the model information received from the API and generates the information to display on the dialog in view mode
         *
         *
         *

        */

        function createDetailFields() {

          return $q(function(resolve, reject) {

                angular.forEach(vm.model, function(config, column){

                  if(config !==null && config.hasOwnProperty("detail_index")){
                    //console.info(config);

                    if(config.type == "LLForeignKey"){
                      var refs = [];
                      var ref_model = "";

                      vm.fields.push({type: config.type, name: column, detail_index:config.detail_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: refs});
                    }
                    else if (config.type == "Set" || config.type == "LLManyToManyField" ) {
                      vm.arrayFields.push({type: config.type,name: column, detail_index:config.detail_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length});
                    }
                    else {
                      vm.fields.push({type: config.type,name: column, detail_index:config.detail_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length});
                    }

                  }
                });

                resolve (vm);
            });

        }


        /**
         * @ngdoc method
         * @name setReference
         * @methodOf app.limalinks.generic-ui.controller:FilterParamsController
         * @description
         * This method sets the reference on the model because ng-model does not provide the ability to be dynamically updated
         *
         *
         *

        */

        function setReference (select){
          vm.object[select.name] = vm.object.ref;
          console.log(select);

        }


        /**
         * @ngdoc method
         * @name generateChildrenSets
         * @methodOf app.limalinks.generic-ui.controller:FilterParamsController
         * @description
         * This method goes through the set fields and matches them with the references arrays to display user friendly lists to the user
         *
         *
         *

        */

        function generateChildrenSets (){
          for(var field in vm.object){
            if(field.substring(field.indexOf("_")+1, field.length) == "set"){
              var index = field.substring(0, field.indexOf("_"))+"s";
              vm.childSetHeadings.push(index);
              var childrenArray = vm[index];
              var children = []; // storing this object's actual children
              angular.forEach(childrenArray, function(child){
                if(child[vm.model.objectType.toLowerCase()+"_ref"]==vm.object.id){
                  children.push(child);


                }
              });

              vm.childSets.push(children);
              vm.object[field] = children;

            }

          }
          console.log(vm.childSets);

        }


        /**
         * @ngdoc method
         * @name activate
         * @methodOf app.limalinks.generic-ui.controller:FilterParamsController
         * @description
         * This is the initialisation method for the controller and it executes all functions that are needed when bootstrapping the view
         *
         *
         *

        */

        function activate() {
            console.info(vm);
            vm.initAddOrEditMode();
            vm.createDetailFields();
            //vm.getReferences();
            vm.controlsResource = vm.createFields();
            vm.controlsResource.then(
              function(){
                console.log("showing modal");
              },
              function(){}
            );







        }

        /**
         * @ngdoc method
         * @name getReferences
         * @methodOf app.limalinks.generic-ui.controller:FilterParamsController
         * @description
         * This method initialises the references for the view from other entities e.g. provinces, districts etc
         *
         *
         *

        */

        function getReferences(referenceField) {
            vm[referenceField+'s'] = [];
              //console.info("model", vm.model);
              vm.resource = UIService.query({},vm.model[referenceField].data_source);
              return vm.resource.then(
                  function(res){

                    vm[referenceField+'s'] = res.data.data;

                  },
                  function(err){

                  }
              )
        }

        /**
         * @ngdoc method
         * @name initAddOrEditMode
         * @methodOf app.limalinks.generic-ui.controller:FilterParamsController
         * @description
         * This method sets the mode to add or edit when the view initialises. It also sets the scope object
         *
         *
         *

        */

        function initAddOrEditMode(){

          //If an item has been selected, then we are in edit mode and we must bind the fields of the view the selected object
          console.log(vm.mode);
          if(vm.mode == "edit"){
            vm.object = vm.selected[0];
            vm.editMode = true;


          }
          else if (vm.mode == "view"){
            vm.object = vm.selected;
            console.log(vm.object);
          }
          else {
            //vm.object = {};
            vm.selected = [];
            vm.editMode = true;
          }
        }

        /**
         * @ngdoc method
         * @name toggleMode
         * @methodOf app.limalinks.generic-ui.controller:FilterParamsController
         * @description
         * This method switches the views mode between add and edit mode
         *
         *
         *

        */

        function toggleMode(){
          if(vm.editMode){
            vm.editMode = false;
          }
          else {
            vm.editMode = true;
          }
        }

        /**
         * @ngdoc method
         * @name hide
         * @methodOf app.limalinks.generic-ui.controller:FilterParamsController
         * @description
         * This method dismisses the modal and returns the filter object to the calling controller to use to query the API
         *
         *
         *

        */

        function hide() {
          console.log(vm.object);
          //vm.object.valid_to = moment(vm.object.valid_to).unix();
          //vm.object.valid_from = moment(vm.object.valid_from).unix();

          angular.forEach(vm.object, function(value, field){

              if(value == "" || value == null || value == []){
                vm.object[field] = undefined;
              }
              console.info(value);
          });

          angular.forEach(vm.chips, function(chip){
            if(vm.object[chip.name] !== undefined){
              vm.object[chip.name] = vm.object[chip.name].map(function(item){
                item = item.id;
                return item;
              }).join();
            }

          });

          angular.forEach(vm.dateControls, function(date){
            date = date.name;
            var paramName = "";
            var startDate = "";
            var endDate = "";
            if (date.indexOf("_start") !==-1){
              console.log(vm.object[date]);
              if(vm.object[date] !== undefined && vm.object[date.replace("_start","_end")] !== undefined ){
                startDate = vm.object[date].toISOString();
                endDate = vm.object[date.replace("_start","_end")].toISOString();
                paramName = date.replace("_start", "");
                var dateParam = startDate+","+endDate;
                vm.object[paramName] = dateParam;
              }
            }
            vm.object[date] = undefined;





          });


          if(vm.mode=="edit"){
            angular.forEach(vm.selects, function(select){
              vm.object[select.name] = vm.object[select.name].id;
            });
          }



          vm.object.depth = 1;  // change the depth of the query
          vm.object.limit = 10;
          vm.object.page = 1;


          $mdDialog.hide(vm.object);
        }

        /**
         * @ngdoc method
         * @name cancel
         * @methodOf app.limalinks.generic-ui.controller:FilterParamsController
         * @description
         * This method dismisses the modal and does not return the scope object to the calling controller
         *
         *
         *

        */

        function cancel() {
            $mdDialog.cancel();
        }
    }
})();
