
(function() {
    'use strict';

    /**
     * @ngdoc controller
     * @name app.limalinks.generic-ui.controller:UIController
     *
     * @description
     * This is a controller for the Objects View
     */


    angular
        .module('app.limalinks.generic-ui')
        .controller('UIController', UIController);

    /* @ngInject */
    function UIController($scope, $timeout, $q, UIService, $mdDialog, $rootScope, $mdToast, AlertService, $state, triLoaderService) {

        //Variable Declaration and Initialisation
        // console.info($state.current.name);

        var vm = this;
        vm.write_access = false;
        vm.objectType = ""; //Used to detect which entity is being viewed
        vm.editData = {}; // Used to send data to Edit Dialog
        vm.columns = []; //Array of master view's columns
        vm.current = {};
        vm.selected = [];
        vm.chartsModel = [];
        vm.chartData = [];
        vm.chartConfigs = [];
        vm.chartOptions = [];
        vm.pageReloadCount = 0;
        vm.query = {
            search: '',
            limit: '10',
            order: 'id',
            page: 1,
            depth: 1
        };
        vm.currentFilter = {};
        vm.filter = {
            options: {
                debounce: 500
            }
        };

        vm.chartConfigs["multiBarChart"] = {
            chart: {
                type: 'multiBarChart',
                height: 300,
                margin : {
                    top: 30,
                    right: 60,
                    bottom: 50,
                    left: 70
                },
                showControls: false,
                noData: 'No data for the selected parameters',
                showValues: true,
                color: d3.scale.category10().range(),
                //useInteractiveGuideline: true,
                duration: 500,
                x: function(d){ return d.label; },
                xAxis: {
                    tickFormat: function(d){
                        return moment(d,"YYYY-MM").format("MMM 'YY");
                    }
                    // axisLabel:"Agents"
                    // tickFormat: function(d){
                    //     return d.label;
                    // }
                },
                yAxis1: {
                    tickFormat: function(d){
                        return d3.format(',.1f')(d);
                    }
                },
                yAxis2: {
                    tickFormat: function(d){
                        return d3.format(',.1f')(d);
                    }
                }

            }
        };

        vm.chartConfigs["lineChart"] = {
            chart: {
                type: 'lineChart',
                height: 300,
                margin : {
                    top: 30,
                    right: 60,
                    bottom: 50,
                    left: 70
                },
                showControls: false,
                noData: 'No data for the selected parameters',
                showValues: true,
                color: d3.scale.category10().range(),
                //useInteractiveGuideline: true,
                x: function(d){ return d.x; },
                y: function(d){ return d.y; },
                duration: 500,
                xAxis: {
                    //axisLabel:"Months"
                    tickFormat: function(d){
                        var label = vm.chartData[vm.chartConfigs["lineChart"].chartId].labels[d];
                        return moment(label,"YYYY-MM-DD").format("MMM DD, YY");
                    }
                },
                yAxis1: {
                    axisLabel:"Farmer Count",
                    tickFormat: function(d){
                        return d3.format(',.1f')(d);
                    }
                },
                yAxis2: {
                    tickFormat: function(d){
                        return d3.format(',.1f')(d);
                    }
                }

            }
        };

        //Controller's API

        vm.activate = activate;
        vm.addObject = addObject;
        vm.createColumns = createColumns;
        vm.deleteObject = deleteObject;
        vm.editObject = editObject;
        vm.viewObject = viewObject;
        vm.getObjects = getObjects;
        vm.getObjectType = getObjectType;
        vm.removeFilter = removeFilter;
        vm.changeParams = changeParams;
        vm.showLoader = showLoader;
        vm.hideLoader = hideLoader;
        vm.filterList = filterList;
        vm.exportData = exportData;
        vm.exportAll = exportAll;
        vm.setAccess = setAccess;


        activate(); //First function called to initialise application.

        ////////////////

        /**
         * @ngdoc method
         * @name activate
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * This method initialises the view by setting default page and querying data
         *
         *
         *
         *

        */

        function activate() {

            vm.setAccess();

            var bookmark;
            $scope.$watch('vm.query.search', function (newValue, oldValue) {

                if(!oldValue) {
                    bookmark = vm.query.page;
                }

                if(newValue !== oldValue) {
                    vm.query.page = 1;
                }

                if(!newValue) {
                    vm.query.page = bookmark;
                }



                vm.getObjectType();

                vm.getObjects();



            });
        }


        /**
         * @ngdoc method
         * @name getObjectType
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * This method detects the type entity being accessed from the application's current state. The controller can then provide specific functionality for the current entity
         *
         *
         *

        */

        function getObjectType(){
          var state = $state;
          var current = state.current.name;
          var objectType = current.substring(current.indexOf("-")+1, current.length);



          switch(objectType) {
              case "provinces":
                  vm.objectType = "Province";
                  break;
              case "districts":
                  vm.objectType = "District";
                  break;
              case "towns":
                  vm.objectType = "Town";
                  break;
              case "wards":
                  vm.objectType = "Ward";
                  break;
              case "markets":
                  vm.objectType = "Market";
                  break;
              case "packagings":
                  vm.objectType = "Packaging";
                  break;
              case "crops":
                  vm.objectType = "Crop";
                  break;
              case "inputs":
                  vm.objectType = "Input";
                  break;
              case "messages":
                  vm.objectType = "Message";
                  break;
              case "admins":
                  vm.objectType = "Admin";
                  break;
              case "farmers":
                  vm.objectType = "Farmer";
                  break;
              case "agents":
                  vm.objectType = "Market Agents";
                  break;
              case "aggregators":
                  vm.objectType = "Aggregator";
                  break;

              case "advertisers":
                  vm.objectType = "Advertiser";
                  break;
              case "transactions":
                  vm.objectType = "Transaction";
                  break;
              case "landmarks":
                  vm.objectType = "Landmark";
                  break;
              default:
                  // console.log("error");
          }

        }



        /**
  			 * @ngdoc method
  			 * @name createColumns
  			 * @methodOf app.limalinks.generic-ui.controller:UIController
  			 * @description
  			 * This methods creates table columns of master view (table)
  			 *
  			 *
         *
         *

  			*/

        function createColumns() {
            //Using pageReloadCount to ensure that this is only executed once to avoid duplication
            if(vm.pageReloadCount==0){
                angular.forEach(vm.model, function(config, column){

                  if(config !==null && config.hasOwnProperty("table_index")){
                    if(config.type == "LLForeignKey"){
                      vm.columns.push({name: column, table_index:config.table_index, label: config.label, type: "ref"});
                    }
                    else if(config.type == "LLManyToManyField"){
                      vm.columns.push({name: column, table_index:config.table_index, label: config.label, type: "array"});
                    }
                    else {
                      vm.columns.push({name: column, table_index:config.table_index, label: config.label, type: "regular"});
                    }
                  }
                });

                vm.pageReloadCount++;
            }



        }


        /**
         * @ngdoc method
         * @name removeFilter
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * This method removes filters and returns view to initial state
         *
         *

        */

        function removeFilter() {
            vm.filter.show = false;
            vm.query.search = '';

            if(vm.filter.form.$dirty) {
                vm.filter.form.$setPristine();
            }
        }

        /**
         * @ngdoc method
         * @name getObjects
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Queries API for view data
         *
         *

        */

        function getObjects() {
            if(vm.pageReloadCount==0){
              vm.showLoader();
            }

            vm.resource = UIService.query(vm.query, "state");

            vm.resource.then(
              function(res){
                vm.hideLoader();
                var objects = res.data;
                //vm.objects = objects.data;

                vm.model = objects.model.fields;
                vm.chartsModel = objects.charts;
                vm.objects = objects.data;
                initialiseChart();

                vm.createColumns();
                vm.editData.model = vm.model;
                vm.editData.tabs = objects.model.tabs;
                vm.editData.model.objectType = vm.objectType; //Tell the generic user controller what type of model this is for naming purposes and configuration

                vm.total_count = objects.total_count;
              },
              function(error){
                //handle error
              }
            );
        }



        /**
         * @ngdoc method
         * @name addObject
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Boardcasts addObject event
         *
         *

        */

        function addObject($event) {
            $rootScope.$broadcast('addObject', $event);
        }

        /**
         * @ngdoc event
         * @name addObject
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Binds addObject event to an action to show Add Object Dialog
         *
         *

        */

        $scope.$on('addObject', function( ev ){
            var templateUrl = "";
            vm.editData.mode = "add";
            if(vm.editData.tabs == undefined){
              templateUrl = 'app/limalinks/generic-ui/add-dialog/add-dialog.tmpl.html';
            }
            else {
              templateUrl = 'app/limalinks/generic-ui/add-dialog/add-wizard.tmpl.html';
            }
            $mdDialog.show({
                locals:{data: vm.editData},
                templateUrl: templateUrl,
                targetEvent: ev,
                controller: 'AddDialogController',
                controllerAs: 'vm'
            })
            .then(function(newObject) {

                vm.resource = UIService.save(newObject, "state");
                vm.resource.then(function(res){
                  var object = res.data;
                  vm.objects.push(object.data);
                  AlertService.showAlert(object.data.name+' has been added', this);
                }, function(error){});
            });
        });

        /**
         * @ngdoc method
         * @name editObject
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Boardcasts editObject event
         *
         *

        */

        function editObject($event) {
            $rootScope.$broadcast('editObject', $event);
        }

        /**
         * @ngdoc event
         * @name editObject
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Binds editObject event to action to show Edit Object Dialog
         *
         *

        */

        $scope.$on('editObject', function( ev ){
            var templateUrl = "";
            vm.editData.selected = vm.selected;
            vm.editData.mode = "edit";
            if(vm.editData.tabs == undefined){
              templateUrl = 'app/limalinks/generic-ui/add-dialog/add-dialog.tmpl.html';
            }
            else {
              templateUrl = 'app/limalinks/generic-ui/add-dialog/add-wizard.tmpl.html';
            }
            $mdDialog.show({
                locals:{data: vm.editData},
                templateUrl: templateUrl,
                targetEvent: ev,
                controller: 'AddDialogController',
                controllerAs: 'vm'
            })
            .then(function(newObject) {
                vm.resource = UIService.update(newObject, "state");
                vm.resource.then(function(res){
                  var object = res.data;
                  vm.objects[vm.objects.indexOf(newObject)] = object.data;
                  vm.selected = [];
                  AlertService.showAlert((object.data.hasOwnProperty("name"))?object.data.name+' has been updated':'Update successful.', this);
                }, function(error){
                    AlertService.showAlert("Error occurred during update. Please try again.");
                });
            },
            function(){
              vm.selected = [];
            }
          );
        });


        /**
         * @ngdoc method
         * @name viewObject
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Boardcasts viewObject event
         *
         *

        */

        function viewObject($event, object) {
            vm.current = object;
            vm.selected = [];
            if(vm.selected.indexOf(object)==0){
              vm.selected.splice(vm.selected.indexOf(object),1);
              vm.selected=object;

            }

            $rootScope.$broadcast('viewObject', $event);
        }

        /**
         * @ngdoc event
         * @name viewObject
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Binds viewObject event to action to show Object details
         *
         *

        */

        $scope.$on('viewObject', function( ev ){
            var templateUrl = "";
            // console.log(vm.selected);
            vm.editData.selected = vm.current;
            vm.editData.mode = "view";
            // console.info(vm.editData.tabs);
            if(vm.editData.tabs == undefined){
              templateUrl = 'app/limalinks/generic-ui/add-dialog/add-dialog.tmpl.html';
            }
            else {
              templateUrl = 'app/limalinks/generic-ui/add-dialog/add-wizard.tmpl.html';
            }
            $mdDialog.show({
                locals:{data: vm.editData},
                templateUrl: templateUrl,
                targetEvent: ev,
                controller: 'AddDialogController',
                controllerAs: 'vm'
            })
            .then(function(newObject) {
                vm.resource = UIService.update(newObject, "state");
                vm.resource.then(function(res){
                  var object = res.data;
                  vm.objects[vm.objects.indexOf(newObject)] = object.data;
                  vm.selected = [];
                  AlertService.showAlert((object.data.hasOwnProperty("name"))?object.data.name+' has been updated':'Update successful.', this);
                }, function(error){
                  AlertService.showAlert("Error occurred during update. Please try again.");
                });
            },
            function(){
              vm.selected = [];
            }
          );
        });


        /**
         * @ngdoc method
         * @name deleteObject
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Broadcasts deleteObject event
         *
         *

        */

        function deleteObject($event) {
            $rootScope.$broadcast('deleteObject', $event);
        }


        /**
         * @ngdoc event
         * @name deleteObject
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Binds deleteObject event to action to show Delete Object Confirmation Dialog
         *
         *

        */
        $scope.$on('deleteObject', function( ev ){
            vm.editData.selected = vm.selected;
            $mdDialog.show({
                locals:{data: vm.editData},
                templateUrl: 'app/limalinks/generic-ui/delete-dialog/delete-dialog.tmpl.html',
                targetEvent: ev,
                controller: 'DeleteDialogController',
                controllerAs: 'vm'
            })
            .then(function(object) {

                //Check all selected objects and delete them by ID
                angular.forEach(vm.selected,function(object){
                    UIService.delete(object, "state")
                     .then(function(res) {
                         vm.objects.splice(vm.objects.indexOf(object),1);
                         vm.selected = [];
                         AlertService.showAlert(object.name+' has been deleted', this);
                     }, function(error) {
                        //  console.log(error);
                     });
                 });

            });
        });



        /**
         * @ngdoc method
         * @name changeParams
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Boardcasts changeParams event
         *
         *
         *
         *

        */

        function changeParams($event, index) {

            vm.model = vm.chartsModel[index];
            vm.model.chart = index;
            $rootScope.$broadcast('changeParams', $event);
        }

        /**
         * @ngdoc event
         * @name changeParams
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Binds changeParams event to action to show Change Params Dialog for Embedded Charts
         *
         *
         *
         *

        */

        $scope.$on('changeParams', function( ev ){
            // console.log(ev);
            $mdDialog.show({
                locals:{data: vm.model},
                templateUrl: 'app/limalinks/analytics-ui/change-params.tmpl.html',
                targetEvent: ev,
                controller: 'ChangeParamsController',
                controllerAs: 'vm'
            })
            .then(function(chart) {
                // console.info("Chart in Controller", chart);

                //vm.chartsModel[vm.model.chart].data = chart.data;
                //vm.chartsModel[vm.model.chart].options = chart.options;
                var labels = [];

                angular.forEach(chart.data[0].values, function(value){
                  labels[value.x]=value.label;
                });
                chart.data.labels = labels;

                vm.chartData[vm.model.chart] = chart.data;
                // console.info("Type", chart.options.type);
                vm.chartConfigs[chart.options.type].params = chart.params;
                vm.chartConfigs[chart.options.type].title = chart.options.title;
                vm.chartOptions[vm.model.chart] = vm.chartConfigs[chart.options.type];





            });
        });




        /**
         * @ngdoc method
         * @name filterList
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Boardcasts filterList event
         *
         *
         *
         *

        */

        function filterList($event, index) {


            $rootScope.$broadcast('filterList', $event);
        }

        /**
         * @ngdoc event
         * @name filterList
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Binds filterList event to action to show Filter Dialog
         *
         *
         *
         *

        */

        $scope.$on('filterList', function( ev ){
            vm.editData.mode = "add";
            vm.editData.filter = vm.currentFilter;
            $mdDialog.show({
                locals:{data: vm.editData},
                templateUrl: 'app/limalinks/generic-ui/filter-dialog/filter-params.tmpl.html',
                targetEvent: ev,
                controller: 'FilterParamsController',
                controllerAs: 'vm'
            })
            .then(function(filter) {
                console.info(filter);
                vm.currentFilter = filter;
                vm.query = vm.currentFilter;
                UIService.query(filter, "state");

                vm.resource = UIService.query(filter, "state");

                vm.resource.then(
                  function(res){
                    vm.hideLoader();
                    var objects = res.data;
                    //vm.objects = objects.data;

                    vm.model = objects.model.fields;
                    vm.chartsModel = objects.charts;
                    vm.objects = objects.data;
                    initialiseChart();

                    vm.createColumns();
                    vm.editData.model = vm.model;
                    vm.editData.model.objectType = vm.objectType; //Tell the generic user controller what type of model this is for naming purposes and configuration

                    vm.total_count = objects.total_count;
                  },
                  function(error){
                    //handle error
                  }
                );








            });
        });


        /**
         * @ngdoc method
         * @name initialiseChart
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Sets initial data of embedded chart
         *
         *
         *
         *

        */
        function initialiseChart(){
            var i = 0;
            angular.forEach(vm.chartsModel, function(aChart){
              // console.log(i);
              var chart = vm.chartsModel[i];

              var chartParams = {}; //will be used to query API
              var chartData = [];
              var params = chart.params;
              var paramInstance = [];
              angular.forEach(params, function(param){
                var index = param.name;
                paramInstance = param.default;
                chartParams[param.name] = paramInstance


              });
              vm.resource = UIService.query(chartParams, aChart.url);
              vm.resource.then(
                function(res){



                  var chart = res.data.data;
                  if(chart.length > 0){
                    var labels = [];

                    angular.forEach(chart.data[0].values, function(value){
                      labels[value.x]=value.label;
                    });
                    chart.data.labels = labels;

                  }


                  vm.chartData[i] = chart.data;
                  vm.chartConfigs[chart.options.type].chartId = i;
                  vm.chartConfigs[chart.options.type].params = chartParams;
                  vm.chartConfigs[chart.options.type].title = chart.options.title;
                  vm.chartOptions[i] = vm.chartConfigs[chart.options.type];
                  // console.log(i);
                  // console.info(vm.chartData[1]);
                  // console.info(vm.chartOptions[1]);
                  i++;

                },
                function(err){

                }
              );

            });










        }



        /**
         * @ngdoc method
         * @name showLoader
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Shows the loader when page is first loaded
         *
         *
         *
         *

        */

        function showLoader() {
            triLoaderService.setLoaderActive(true);
        }

        /**
         * @ngdoc method
         * @name showLoader
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Hides the loader after data is completely loaded
         *
         *
         *
         *

        */

        function hideLoader(){
            triLoaderService.setLoaderActive(false);
        }



        /**
         * @ngdoc method
         * @name exportData
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Exports selected data to Excel using alasql
         *
         *
         *
         *

        */
        function exportData(){

            var dataToExport = vm.selected;
            alasql('SELECT * INTO XLSX("export.xlsx",{headers:true}) FROM ?',[dataToExport] );
            AlertService.showAlert("Your selected data has been exported to Excel. Please check your Downloads directory.", this);
        }

        /**
         * @ngdoc method
         * @name exportAll
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Exports all data without filters to Excel using alasql
         *
         *
         *
         *

        */
        function exportAll(){

           var dataToExport = [];

           vm.resource = UIService.query({}, "state");
           vm.resource.then(
             function(res){
               dataToExport = res.data.data;
               alasql('SELECT * INTO XLSX("export.xlsx",{headers:true}) FROM ?',[dataToExport] );
               AlertService.showAlert("Data has been exported to Excel. Please check your Downloads directory.", this);
             },
             function (error){

             }
           )

        }



        /**
         * @ngdoc method
         * @name setAccess
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Provides access to controls that can be used for data manipulation.
         *
         *
         *
         *

        */
        function setAccess(){
            var views = $rootScope.user.views;
            var state = $state.current.name;
            for (var i=0; i<views.length; i++){
              if (state == views[i].state){
                (views[i].hasOwnProperty("write_access") && views[i].write_access == true)?vm.write_access = true : vm.write_access = false;
              }
            }

        }












    }
})();
