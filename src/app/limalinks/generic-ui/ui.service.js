(function() {
    'use strict';

    /**
     * @ngdoc controller
     * @name app.limalinks.generic-ui.service:UIService
     *
     * @description
     * This is a service that interacts with generic APIService in limalinks-api.service.js service to provide data to the UIController
     */


    angular
        .module('app.limalinks.generic-ui')
        .service('UIService', UIService);

    /* @ngInject */
    function UIService($q, $http, SessionService, limalinksSettings, $resource, $cookies, APIService, $state, $rootScope){

        /**
         * @ngdoc method
         * @name query
         * @methodOf app.limalinks.generic-ui.service:UIService
         * @description
         * This method queries data using APIService
         *
         *
         *

        */

        this.query = function(object, type){
          return APIService.query(object, getEndPoint(type));
        }


        /**
         * @ngdoc method
         * @name save
         * @methodOf app.limalinks.generic-ui.service:UIService
         * @description
         * This method saves new data using APIService
         *
         *
         *

        */

        this.save = function(object, type){
          return APIService.save(object, getEndPoint(type));
        }


        /**
         * @ngdoc method
         * @name update
         * @methodOf app.limalinks.generic-ui.service:UIService
         * @description
         * This method updates existing data using APIService
         *
         *
         *

        */

        this.update = function(object, type){
          return APIService.update(object, getEndPoint(type));
        }



        /**
         * @ngdoc method
         * @name delete
         * @methodOf app.limalinks.generic-ui.service:UIService
         * @description
         * This method deletes data using APIService
         *
         *
         *

        */

        this.delete = function(object, type){
          return APIService.delete(object, getEndPoint(type));
        }


        /**
         * @ngdoc method
         * @name getEndPoint
         * @methodOf app.limalinks.generic-ui.service:UIService
         * @description
         * This method sets specific endpoint for this entity
         *
         *
         *

        */

        function getEndPoint(type){
          var objectType = "";
          var endPoint = "";
          if (type=="state"){
            var state = $state;
            var current = state.current.name;

            $rootScope.views.forEach(function(view){

              if (view.state == current){

                endPoint = view.url;
              }
            });
            return endPoint;
          }
          else {
            return objectType = type;

          }
        }

  }

})();
