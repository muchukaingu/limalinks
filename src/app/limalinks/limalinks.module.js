(function() {
    'use strict';

    angular
        .module('app.limalinks', [
            'app.limalinks.generic-ui',
            'app.limalinks.analytics-ui',
            'app.limalinks.authentication',
            'app.limalinks.users'

        ]);
})();
