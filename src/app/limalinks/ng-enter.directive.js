(function() {
    'use strict';

    /**
     * @ngdoc directive
     * @name app.limalinks.directive:ngEnter
     *
     * @description
     * This directive allows us to pass a function in on an enter key to do what we want.
     */

    angular
        .module('app.limalinks')
        .directive('ngEnter', ngEnter);

    /* @ngInject */
    function ngEnter() {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    }

})();
