
(function() {
    'use strict';

    /**
     * @ngdoc controller
     * @name app.limalinks.users.controller:UsersController
     *
     * @description
     * This is a controller for the Users View
     */


    angular
        .module('app.limalinks.users')
        .controller('UsersController', UsersController);

    /* @ngInject */
    function UsersController($scope, $timeout, $q, UsersService, $mdDialog, $rootScope, AlertService, $state, $mdToast, Papa, ChartService, $http, UIService) {

        var vm = this;
        vm.charts = [];
        vm.userType = "";
        vm.editData = {}; // Used to send data to Edit Dialog
        vm.chartsModel = [];
        vm.charts = [];
        vm.chartData = [];
        vm.chartOptions = [];
        vm.chartConfigs = [];



        //Controller's API

        vm.activate = activate;
        vm.addUser = addUser;
        vm.changeParams = changeParams;
        vm.deleteUser = deleteUser;
        vm.editUser = editUser;
        vm.getUsers = getUsers;
        vm.removeFilter = removeFilter;
        vm.getUserType = getUserType;
        vm.uploadUsers = uploadUsers;
        vm.generateCharts = generateCharts;
        vm.generateChartParams = generateChartParams;


        vm.query = {
            search: '',
            limit: '10',
            order: '-id',
            page: 1
        };

        vm.selected = [];

        vm.columns = {

            id: 'ID',
            first_name: 'First Name',
            last_name: 'Last Name',
            address: 'Address',
            email: 'Email',
            phone_number: 'Phone Number',
            date_created: 'Date Created',
            last_updated: 'Last Updated'

        };

        vm.chartConfigs["multiBarChart"] = {
            chart: {
                type: 'multiBarChart',
                height: 300,
                margin : {
                    top: 30,
                    right: 60,
                    bottom: 50,
                    left: 70
                },
                showControls: false,
                noData: 'Change Parameters to View Data',
                showValues: true,
                color: d3.scale.category10().range(),
                //useInteractiveGuideline: true,
                duration: 500,
                x:function(d){return d.label;},
                xAxis: {
                  tickFormat: function(d){

                      return moment(d,"YYYY-MM").format("MMM 'YY");
                  }


                },
                yAxis1: {
                    tickFormat: function(d){
                        return d3.format(',.1f')(d);
                    }
                },
                yAxis2: {
                    tickFormat: function(d){
                        return d3.format(',.1f')(d);
                    }
                }

            }
        };

        vm.chartConfigs["lineChart"] = {
            chart: {
                type: 'lineChart',
                height: 300,
                margin : {
                    top: 30,
                    right: 60,
                    bottom: 50,
                    left: 70
                },
                showControls: false,
                noData: 'Change Parameters to View Data',
                showValues: true,
                color: d3.scale.category10().range(),
                //useInteractiveGuideline: true,
                x: function(d){ return d.x; },
                y: function(d){ return d.y; },
                duration: 500,
                xAxis: {
                    tickFormat: function(d){
                        var label = vm.chartData[vm.chartConfigs["lineChart"].chartId].labels[d];
                        return moment(label,"YYYY-MM-DD").format("MMM DD, YY");
                    }
                },
                yAxis1: {
                    axisLabel:"Farmer Count",
                    tickFormat: function(d){
                        return d3.format(',.1f')(d);
                    }
                },
                yAxis2: {
                    tickFormat: function(d){
                        return d3.format(',.1f')(d);
                    }
                }

            }
        };




        vm.filter = {
            options: {
                debounce: 500
            }
        };


        activate();

        ////////////////

        function getUserType(){
          var state = $state;
          var current = state.current.name;
          var userType = current.substring(current.indexOf("-")+1, current.length);



          switch(userType) {
              case "administrators":
                  vm.userType = "Administrator";
                  break;
              case "farmers":
                  vm.userType = "Farmer";
                  break;
              case "agents":
                  vm.userType = "Agent";
                  break;
              case "advertisers":
                  vm.userType = "Advertiser";
                  break;
              case "aggregators":
                  vm.userType = "Aggregator";
                  break;
              case "analytics":
                  vm.userType = "Analytic";
                  break;
              default:
                  console.log("error");
          }

        }


        /**
  			 * @ngdoc method
  			 * @name activate
  			 * @methodOf app.limalinks.users.controller:UsersController
  			 * @description
  			 * This method initialises the view by setting default page and querying data
  			 *
  			 *
         *
         *

  			*/

        function activate() {
            var bookmark;
            $scope.$watch('vm.query.search', function (newValue, oldValue) {

                if(!oldValue) {
                    bookmark = vm.query.page;
                }

                if(newValue !== oldValue) {
                    vm.query.page = 1;
                }

                if(!newValue) {
                    vm.query.page = bookmark;
                }
                vm.getUserType();
                vm.getUsers();

            });

            $scope.$watch('vm.selected.length', function (newValue, oldValue) {

                console.info("selected", vm.selected);
            });
        }

        /**
  			 * @ngdoc method
  			 * @name generateCharts
  			 * @methodOf app.limalinks.users.controller:UsersController
  			 * @description
  			 * generates chart data for the view
  			 *
  			 *
         *
         *

  			*/

        function generateCharts(charts) {
          ChartService.generateData(charts);
        }


        /**
  			 * @ngdoc method
  			 * @name generateChartParams
  			 * @methodOf app.limalinks.users.controller:UsersController
  			 * @description
  			 * generates chart data for the view
  			 *
  			 *
         *
         *

  			*/

        function generateChartParams(charts) {
          ChartService.generateData(charts);
        }



        /**
         * @ngdoc method
         * @name removeFilter
         * @methodOf app.limalinks.users.controller:UsersController
         * @description
         * This method removes filters and returns view to initial state
         *
         *
         *
         *

        */

        function removeFilter() {
            vm.filter.show = false;
            vm.query.search = '';

            if(vm.filter.form.$dirty) {
                vm.filter.form.$setPristine();
            }
        }

        /**
         * @ngdoc method
         * @name getUsers
         * @methodOf app.limalinks.users.controller:UsersController
         * @description
         * Queries API for view data
         *
         *
         *
         *

        */

        function getUsers() {

            vm.resource = UsersService.query(vm.query, "state");

            vm.resource.then(
              function(res){
                var users = res.data;
                vm.users = users.data;
                vm.editData.model = users.model;
                vm.editData.model.name = vm.userType; //Tell the generic user controller what type of model this is for naming purposes and configuration
                vm.total_count = users.total_count;
                vm.chartsModel = users.charts;
                angular.forEach(vm.chartsModel, function(chart){
                  chart.data = [];
                });
                initialiseChart();
                //generateCharts(vm.charts);

              },
              function(error){
                //handle error
              }
            );
        }

        /**
         * @ngdoc method
         * @name addUser
         * @methodOf app.limalinks.users.controller:UsersController
         * @description
         * Boardcasts addUser event
         *
         *
         *
         *

        */

        function addUser($event) {
            $rootScope.$broadcast('addUser', $event);
        }

        /**
         * @ngdoc event
         * @name addUser
         * @methodOf app.limalinks.users.controller:UsersController
         * @description
         * Binds addUser event to action
         *
         *
         *
         *

        */

        $scope.$on('addUser', function( ev ){

            $mdDialog.show({
                locals:{model: vm.editData.model},
                templateUrl: 'app/limalinks/users/templates/add-user-dialog.tmpl.html',
                targetEvent: ev,
                controller: 'AddUserDialogController',
                controllerAs: 'vm'
            })
            .then(function(newUser) {

                vm.resource = UsersService.save(newUser, "state");
                vm.resource.then(function(res){
                  var user = res.data;
                  console.log(user.data);
                  vm.users.push(user.data);
                  AlertService.showAlert('User '+user.data.first_name+' '+user.data.last_name+' has been added', this);
                }, function(error){});
            });
        });

        /**
         * @ngdoc method
         * @name editUser
         * @methodOf app.limalinks.users.controller:UsersController
         * @description
         * Boardcasts editUser event
         *
         *
         *
         *

        */

        function editUser($event) {
            $rootScope.$broadcast('editUser', $event);
        }

        /**
         * @ngdoc event
         * @name editUser
         * @methodOf app.limalinks.users.controller:UsersController
         * @description
         * Binds editUser event to action
         *
         *
         *
         *

        */

        $scope.$on('editUser', function( ev ){
            vm.editData.selected = vm.selected;
            $mdDialog.show({
                locals:{data: vm.editData},
                templateUrl: 'app/limalinks/users/templates/add-user-dialog.tmpl.html',
                targetEvent: ev,
                controller: 'EditUserDialogController',
                controllerAs: 'vm'
            })
            .then(function(newUser) {
                vm.resource = UsersService.update(newUser, "state");
                vm.resource.then(function(res){
                  var user = res.data;
                  vm.users[vm.users.indexOf(newUser)] = user.data;
                  vm.selected = [];
                  console.log(user.data);
                  AlertService.showAlert('User '+user.data.first_name+' '+user.data.last_name+' has been updated', this);
                }, function(error){});
            });
        });


        /**
         * @ngdoc method
         * @name deleteUser
         * @methodOf app.limalinks.users.controller:UsersController
         * @description
         * Broadcasts deleteUser event
         *
         *
         *
         *

        */

        function deleteUser($event) {
            $rootScope.$broadcast('deleteUser', $event);
        }


        /**
         * @ngdoc event
         * @name deleteUser
         * @methodOf app.limalinks.users.controller:UsersController
         * @description
         * Binds deleteUser event to action
         *
         *
         *
         *

        */
        $scope.$on('deleteUser', function( ev ){

            $mdDialog.show({
                locals:{selected: vm.selected},
                templateUrl: 'app/limalinks/users/templates/delete-user-dialog.tmpl.html',
                targetEvent: ev,
                controller: 'DeleteUserDialogController',
                controllerAs: 'vm'
            })
            .then(function(user) {

                //Check all selected users and delete them by ID
                angular.forEach(vm.selected,function(user){
                    vm.resource = UsersService.delete(user, "state");
                     vm.resource.then(function(res) {
                         vm.users.splice(vm.users.indexOf(user),1);
                         vm.selected = [];
                         AlertService.showAlert('User '+user.first_name+' '+user.last_name+' has been deleted', this);
                     }, function(error) {
                         console.log(error);
                     });
                 });

            });
        });


        function uploadUsers($files) {
            if($files !== null && $files.length > 0) {
                var message = 'Thanks for ';
                for(var file in $files) {
                    message += $files[file].name + ' ';
                    Papa.parse($files[file], {header:true})
                    .then(function(result){
                      return $q(function(resolve, reject) {
                          var count = 0;
                          angular.forEach(result.data, function(user){
                              console.log(user.user_type.toLowerCase());
                              vm.resource = UsersService.save(user, user.user_type.toLowerCase()+"s");
                              vm.resource.then(function(res){
                                var user = res.data;
                                console.log(user.data);
                                vm.users.push(user.data);
                                if (count == result.data.length){
                                  resolve(function(){
                                    AlertService.showAlert(count+' users have been uploaded from '+ $files[file].name, this);
                                  });
                                }
                                count++;
                                //AlertService.showAlert('User '+user.data.first_name+' '+user.data.last_name+' has been added', this);
                              }, function(error){});
                          });
                        });
                    })
                    .catch(function(){})
                    .finally(function(){});
                }

            }
        }


        /**
         * @ngdoc method
         * @name addUser
         * @methodOf app.limalinks.users.controller:UsersController
         * @description
         * Boardcasts addUser event
         *
         *
         *
         *

        */

        function changeParams($event, index) {

            vm.model = vm.chartsModel[index];
            vm.model.chart = index;
            $rootScope.$broadcast('changeParams', $event);
        }

        /**
         * @ngdoc event
         * @name changeParams
         * @methodOf app.limalinks.users.controller:UsersController
         * @description
         * Binds changeParams event to action
         *
         *
         *
         *

        */

        $scope.$on('changeParams', function( ev ){
            console.log(ev);
            $mdDialog.show({
                locals:{data: vm.model},
                templateUrl: 'app/limalinks/analytics-ui/change-params.tmpl.html',
                targetEvent: ev,
                controller: 'ChangeParamsController',
                controllerAs: 'vm'
            })
            .then(function(chart) {
              console.info("Chart in Controller", chart);

              //vm.chartsModel[vm.model.chart].data = chart.data;
              //vm.chartsModel[vm.model.chart].options = chart.options;
              var labels = [];

              angular.forEach(chart.data[0].values, function(value){
                labels[value.x]=value.label;
              });
              chart.data.labels = labels;

              vm.chartData[vm.model.chart] = chart.data;
              console.info("Params", chart.params);
              vm.chartConfigs[chart.options.type].params = chart.params;
              vm.chartConfigs[chart.options.type].title = chart.options.title;
              vm.chartOptions[vm.model.chart] = vm.chartConfigs[chart.options.type];





            });
        });




        /**
         * @ngdoc method
         * @name initialiseChart
         * @methodOf app.limalinks.generic-ui.controller:UIController
         * @description
         * Sets initial data of embedded chart
         *
         *
         *
         *

        */

        function initialiseChart(){
            var i = 0;
            angular.forEach(vm.chartsModel, function(aChart){
              console.log(i);
              var chart = vm.chartsModel[i];

              var chartParams = {}; //will be used to query API
              var chartData = [];
              var params = chart.params;
              var paramInstance = [];
              angular.forEach(params, function(param){
                var index = param.name;
                paramInstance = param.default;
                chartParams[param.name] = paramInstance


              });
              vm.resource = UIService.query(chartParams, aChart.url);
              vm.resource.then(
                function(res){



                  var chart = res.data.data;

                  var labels = [];

                  angular.forEach(chart.data[0].values, function(value){
                    labels[value.x]=value.label;
                  });
                  chart.data.labels = labels;

                  vm.chartData[i] = chart.data;
                  vm.chartConfigs[chart.options.type].params = chartParams;
                  vm.chartConfigs[chart.options.type].title = chart.options.title;
                  vm.chartOptions[i] = vm.chartConfigs[chart.options.type];
                  console.log(i);
                  console.info(vm.chartData[1]);
                  console.info(vm.chartOptions[1]);
                  i++;

                },
                function(err){

                }
              );

            });










        }

















    }
})();
