NG_DOCS={
  "sections": {
    "Documentation": "Web App Documentation"
  },
  "pages": [
    {
      "section": "Documentation",
      "id": "AdminController",
      "shortName": "AdminController",
      "type": "function",
      "moduleName": "triAngular",
      "shortDescription": "Handles the admin view",
      "keywords": "admin admincontroller documentation function handles triangular view"
    },
    {
      "section": "Documentation",
      "id": "AdminController",
      "shortName": "AdminController",
      "type": "function",
      "moduleName": "triAngular",
      "shortDescription": "Handles the admin view",
      "keywords": "admin admincontroller documentation function handles triangular view"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.directive:dynamicModel",
      "shortName": "dynamicModel",
      "type": "directive",
      "moduleName": "app.limalinks",
      "shortDescription": "This directive creates a dynamic ngModel",
      "keywords": "app creates directive documentation dynamic dynamicmodel limalinks ngmodel"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.generic-ui.controller:AddObjectDialogController",
      "shortName": "AddObjectDialogController",
      "type": "controller",
      "moduleName": "app.limalinks.generic-ui",
      "shortDescription": "This is a controller for the dialog for adding Objects",
      "keywords": "ability activate add adding addobjectdialogcontroller api app arrays bootstrapping calling cancel controller controls createdetailfields createfields dialog dismisses display districts documentation dynamically edit entities executes fields friendly functions generatechildrensets generates generic-ui getreferences hide initaddoreditmode initialisation initialises limalinks lists matches method modal mode model needed ng-model object objects provide provinces received reference references return returns scope set setreference sets switches togglemode traverses ui updated user view views"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.generic-ui.controller:ChangeParamsController",
      "shortName": "ChangeParamsController",
      "type": "controller",
      "moduleName": "app.limalinks.generic-ui",
      "shortDescription": "This is a controller for controlling Parameter and Chart Data Generation",
      "keywords": "app changeparamscontroller chart controller controlling data documentation generation generic-ui limalinks parameter"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.generic-ui.controller:ChipController",
      "shortName": "ChipController",
      "type": "controller",
      "moduleName": "app.limalinks.generic-ui",
      "shortDescription": "This is a generic controller for chips controls",
      "keywords": "300ms app async chipcontroller chips controller controls create debounce debouncesearch delayedquerysearch documentation faster filter function generic generic-ui limalinks md-object-chips method objects queries query querying querysearch returned search searchs string support"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.generic-ui.controller:DeleteDialogController",
      "shortName": "DeleteDialogController",
      "type": "controller",
      "moduleName": "app.limalinks.generic-ui",
      "shortDescription": "This is a controller for the the deletion confirmation dialog",
      "keywords": "app cancels confirmation confirms controller deletedialogcontroller deletion dialog documentation generic-ui hide hides items limalinks method selected"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.generic-ui.controller:FilterParamsController",
      "shortName": "FilterParamsController",
      "type": "controller",
      "moduleName": "app.limalinks.generic-ui",
      "shortDescription": "This is a controller for the dialog for filtering Objects",
      "keywords": "ability activate add api app arrays bootstrapping calling cancel controller controls createdetailfields createfields dialog dismisses display districts documentation dynamically edit entities executes fields filter filtering filterparamscontroller filters friendly functions generatechildrensets generates generic-ui getreferences hide initaddoreditmode initialisation initialises limalinks lists matches method modal mode model needed ng-model object objects provide provinces query received reference references return returns scope set setreference sets switches togglemode traverses ui updated user view views"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.generic-ui.controller:UIController",
      "shortName": "UIController",
      "type": "controller",
      "moduleName": "app.limalinks.generic-ui",
      "shortDescription": "This is a controller for the Objects View",
      "keywords": "accessed action activate add addobject alasql api app application binds boardcasts broadcasts change changeparams chart charts columns completely confirmation controller createcolumns creates current data default delete deleteobject details detects dialog documentation edit editobject embedded entity event excel exportall exportdata exports filter filterlist filters functionality generic-ui getobjects getobjecttype hides initial initialisechart initialises limalinks loaded loader master method methods object objects params provide queries querying removefilter removes returns selected sets setting showloader specific table type uicontroller view viewobject"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.generic-ui.service:UIService",
      "shortName": "UIService",
      "type": "controller",
      "moduleName": "app.limalinks.generic-ui",
      "shortDescription": "This is a service that interacts with generic APIService in limalinks-api.service.js service to provide data to the UIController",
      "keywords": "apiservice app controller data delete deletes documentation endpoint entity existing generic generic-ui getendpoint interacts js limalinks limalinks-api method provide queries query save saves service sets specific uicontroller uiservice update updates"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.service:AlertService",
      "shortName": "AlertService",
      "type": "service",
      "moduleName": "app.limalinks",
      "shortDescription": "This service provides high-level abstraction for all API calls.",
      "keywords": "abstraction alertservice api app bottom calling calls creates display documentation high-level limalinks mdtoast message method object parent provided service showalert toast"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.service:AlertService",
      "shortName": "AlertService",
      "type": "service",
      "moduleName": "app.limalinks",
      "shortDescription": "This service provides high-level abstraction for all API calls.",
      "keywords": "abstraction alertservice api app calls documentation high-level limalinks service"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.service:APIService",
      "shortName": "APIService",
      "type": "service",
      "moduleName": "app.limalinks",
      "shortDescription": "This service provides high-level abstraction for all API calls.",
      "keywords": "$http abstraction api apiservice app blank calls create creating delete deleting documentation entities high-level instances leave limalinks method object objects promise queries query retrieve return save service specific update updating"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.service:ChipService",
      "shortName": "ChipService",
      "type": "service",
      "moduleName": "app.limalinks",
      "shortDescription": "This service provides high-level abstraction for all API calls creating chips",
      "keywords": "abstraction api app calls chips chipservice creating documentation high-level limalinks service"
    },
    {
      "section": "Documentation",
      "id": "app.limalinks.users.controller:UsersController",
      "shortName": "UsersController",
      "type": "controller",
      "moduleName": "app.limalinks.users",
      "shortDescription": "This is a controller for the Users View",
      "keywords": "action activate adduser api app binds boardcasts broadcasts changeparams chart controller data default deleteuser documentation edituser event filters generatechartparams generatecharts generates getusers initial initialises limalinks method queries querying removefilter removes returns setting users userscontroller view"
    }
  ],
  "apis": {
    "Documentation": true
  },
  "__file": "_FAKE_DEST_/js/docs-setup.js",
  "__options": {
    "startPage": "/api",
    "scripts": [
      "js/angular.min.js",
      "js/angular-animate.min.js",
      "js/marked.js"
    ],
    "styles": [],
    "title": "API Documentation",
    "html5Mode": true,
    "editExample": true,
    "navTemplate": false,
    "navContent": "",
    "navTemplateData": {},
    "loadDefaults": {
      "angular": true,
      "angularAnimate": true,
      "marked": true
    }
  },
  "html5Mode": true,
  "editExample": true,
  "startPage": "/api",
  "scripts": [
    "js/angular.min.js",
    "js/angular-animate.min.js",
    "js/marked.js"
  ]
};